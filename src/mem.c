#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region *r);


static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {
    // get size and bytes
    block_size blk_size = size_from_capacity((block_capacity){.bytes = query});
    size_t blk_bytes = blk_size.bytes;
    // get size and alloc
    size_t reg_size = region_actual_size(blk_bytes);
    void* alloc = map_pages(addr,reg_size,MAP_FIXED_NOREPLACE);
    bool is_failed = alloc == MAP_FAILED;
    // alloc again
    if (is_failed) {
        alloc = map_pages(addr,reg_size,0);
        bool is_failed_again = alloc == MAP_FAILED;
        if (is_failed_again) {
            return REGION_INVALID;
        }
    }
    // create region
    bool is_extend = alloc == addr;
    struct region region = {0};
    region.addr = alloc;
    region.extends = is_extend;
    region.size = reg_size;
    // init block and return
    block_size alloc_blk_size = {.bytes = region.size};
    block_init(region.addr,alloc_blk_size,0);
    return region;
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
    // check is splittable
    bool can_split = block_splittable(block, query);
    if (can_split) {
        // calc addr and size
        void* new_blk_addr = block->contents;
        new_blk_addr += query;
        block_size new_blk_size = {0};
        new_blk_size.bytes = block->capacity.bytes - query;
        // init block and return
        block_init(new_blk_addr, new_blk_size, block->next);
        block->next = new_blk_addr;
        block->capacity.bytes = query;
        return true;
    } else {
        return false;
    }
}


/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {

    // get neighbor
    struct block_header* neighbor = block->next;
    // null check
    bool is_null = neighbor == NULL;
    if (is_null) {
        return false;
    }
    // merge-able check
    bool can_merge = mergeable(block, neighbor);
    if (can_merge) {
        // merge
        struct block_header* new_neighbor = neighbor->next;
        block_size additional = size_from_capacity(neighbor->capacity);
        block->capacity.bytes = block->capacity.bytes + additional.bytes;
        // change next and return
        block->next = new_neighbor;
        return true;
    } else {
        return false;
    }
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};


static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    struct block_header* cur_block = block;
    bool is_null = cur_block == NULL;
    if (is_null) {
        struct block_search_result result = {0};
        result.type = BSR_CORRUPTED;
        result.block = cur_block;
        return result;
    }
    while (!is_null) {
        // check is free
        bool is_free = cur_block->is_free;
        if (is_free) {
            // try merge
            while (try_merge_with_next(cur_block)) {
                // merge me full
            }
            // if big enough - return
            bool is_big = block_is_big_enough(sz, cur_block);
            if (is_big) {
                struct block_search_result result = {0};
                result.type = BSR_FOUND_GOOD_BLOCK;
                result.block = cur_block;
                return result;
            }
        }
        // get next block
        bool can_next = cur_block->next != NULL;
        if (can_next) {
            cur_block = cur_block->next;
            is_null = cur_block == NULL;
        } else {
            break;
        }
    }
    // return end
    struct block_search_result result = {0};
    result.type = BSR_REACHED_END_NOT_FOUND;
    result.block = cur_block;
    return result;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    // find good or last
    struct block_search_result res = find_good_or_last(block, query);
    // check is good
    bool find_good = res.type == BSR_FOUND_GOOD_BLOCK;
    if (find_good) {
        // split and return
        split_if_too_big(res.block, query);
        res.block->is_free = false;
        return res;
    } else {
        return res;
    }
}


static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    bool is_null = last == NULL;
    if (is_null) {
        return NULL;
    }
    // get new addr
    void* new_reg_addr = block_after(last);
    // alloc region
    struct region reg = alloc_region(new_reg_addr, query);
    bool is_invalid = region_is_invalid(&reg);
    if (!is_invalid) {
        // update last
        last->next = reg.addr;
        // try merge
        bool merged = try_merge_with_next(last);
        if (merged) {
            return last;
        } else {
            return reg.addr;
        }
    } else {
        return NULL;
    }
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    // get real query
    size_t real_query = size_max(BLOCK_MIN_CAPACITY, query);
    // try alloc
    struct block_search_result result = try_memalloc_existing(real_query, heap_start);
    // if success - return
    bool is_success = result.type == BSR_FOUND_GOOD_BLOCK;
    if (is_success) {
        return result.block;
    } else {
        // if reach end - try to grow heap
        bool is_end = result.type == BSR_REACHED_END_NOT_FOUND;
        if (is_end) {
            struct block_header* new = grow_heap(result.block, real_query);
            bool is_null = new == NULL;
            if (!is_null) {
                // try alloc again
                result = try_memalloc_existing(real_query, new);
                return result.block;
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }
}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    try_merge_with_next(header);
}
