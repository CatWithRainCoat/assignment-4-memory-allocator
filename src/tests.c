#include "tests.h"

void print_error(char* message) {
    fprintf(stderr, "%s", message);
}

void print_success(char* message) {
    fprintf(stdout, "%s", message);
}

void test1() {
    void* heap = heap_init(5000);
    bool is_null = heap == NULL;
    if (is_null) {
        print_error("Heap not init");
        return;
    }
    debug_heap(stdout, heap);
    void* alloc = _malloc(2000);
    debug_heap(stdout, heap);
    is_null = alloc == NULL;
    if (is_null) {
        print_error("Malloc failed");
        return;
    }
    _free(alloc);
    debug_heap(stdout, heap);
    munmap(heap, 5000);
    print_success("Test 1 passed");
}

void test2() {
    void* heap = heap_init(5000);
    bool is_null = heap == NULL;
    if (is_null) {
        print_error("Heap not init");
        return;
    }
    debug_heap(stdout, heap);
    void* alloc1 = _malloc(512);
    void* alloc2 = _malloc(512);
    void* alloc3 = _malloc(512);
    void* alloc4 = _malloc(512);
    debug_heap(stdout, heap);

    _free(alloc2);
    _free(alloc4);
    debug_heap(stdout, heap);

    _free(alloc1);
    _free(alloc3);

    munmap(heap, 5000);
    print_success("Test 2 passed");
}

void test3() {
    void* heap = heap_init(5000);
    bool is_null = heap == NULL;
    if (is_null) {
        print_error("Heap not init");
        return;
    }
    debug_heap(stdout, heap);
    void* alloc1 = _malloc(512);
    void* alloc2 = _malloc(512);
    void* alloc3 = _malloc(512);
    void* alloc4 = _malloc(512);
    void* alloc5 = _malloc(512);
    void* alloc6 = _malloc(512);
    debug_heap(stdout, heap);

    _free(alloc2);
    _free(alloc4);
    _free(alloc6);
    debug_heap(stdout, heap);

    _free(alloc5);
    _free(alloc3);
    _free(alloc1);
    debug_heap(stdout, heap);

    munmap(heap, 5000);
    print_success("Test 3 passed");
}

void test4() {
    void* heap = heap_init(5000);
    bool is_null = heap == NULL;
    if (is_null) {
        print_error("Heap not init");
        return;
    }
    debug_heap(stdout, heap);
    void* alloc = _malloc(10000);
    debug_heap(stdout, heap);
    is_null = alloc == NULL;
    if (is_null) {
        print_error("Malloc failed");
        return;
    }

    struct block_header* heap_header = (struct block_header*) heap;
    bool is_grow = heap_header->capacity.bytes >= 10000;
    if (!is_grow) {
        print_error("Head not grow");
        return;
    }

    _free(alloc);
    debug_heap(stdout, heap);

    munmap(heap, 10000);
    print_success("Test 4 passed");
}

void test5() {
    void* heap = heap_init(5000);
    bool is_null = heap == NULL;
    if (is_null) {
        print_error("Heap not init");
        return;
    }
    debug_heap(stdout, heap);

    void* wall = mmap(heap + 5000, 1000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);

    void* alloc = _malloc(10000);
    debug_heap(stdout, heap);
    is_null = alloc == NULL;
    if (is_null) {
        print_error("Malloc failed");
        return;
    }

    struct block_header* heap_header = (struct block_header*) heap;
    bool is_success = heap_header->is_free && !heap_header->next->is_free;
    if (!is_success) {
        print_error("Head not grow");
        return;
    }

    _free(alloc);
    debug_heap(stdout, heap);

    munmap(heap, 5000);
    munmap(wall, 1000);
    print_success("Test 5 passed");
}